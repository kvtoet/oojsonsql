<?php
//ini_set('display_errors', 'Off');
//error_reporting(0);

require('database.inc.php');

function outputCSV($data) {

    $outstream = fopen("php://output", 'w');

    function __outputCSV(&$vals, $key, $filehandler) {
        fputcsv($filehandler, $vals, ';', '"');
    }
    array_walk($data, '__outputCSV', $outstream);

    fclose($outstream);
}

function prettyPrint( $json )
{
    $result = '';
    $level = 0;
    $prev_char = '';
    $in_quotes = false;
    $ends_line_level = NULL;
    $json_length = strlen( $json );

    for( $i = 0; $i < $json_length; $i++ ) {
        $char = $json[$i];
        $new_line_level = NULL;
        $post = "";
        if( $ends_line_level !== NULL ) {
            $new_line_level = $ends_line_level;
            $ends_line_level = NULL;
        }
        if( $char === '"' && $prev_char != '\\' ) {
            $in_quotes = !$in_quotes;
        } else if( ! $in_quotes ) {
            switch( $char ) {
                case '}': case ']':
                    $level--;
                    $ends_line_level = NULL;
                    $new_line_level = $level;
                    break;

                case '{': case '[':
                    $level++;
                case ',':
                    $ends_line_level = $level;
                    break;

                case ':':
                    $post = " ";
                    break;

                case " ": case "\t": case "\n": case "\r":
                    $char = "";
                    $ends_line_level = $new_line_level;
                    $new_line_level = NULL;
                    break;
            }
        }
        if( $new_line_level !== NULL ) {
            $result .= "\n".str_repeat( "\t", $new_line_level );
        }
        $result .= $char.$post;
        $prev_char = $char;
    }

    return $result;
}

function output() {
	$prettyPrint = false;
	$isCSV = false;

	if (isset($_REQUEST['json'])) {
		$decoded = json_decode(stripslashes($_REQUEST['json']), TRUE);
		
		if (isset($decoded['prettyJSON']))
			$prettyPrint = true;
			
		if (isset($decoded['isCSV']) && $decoded['isCSV'])
			$isCSV = true;
		
		if (is_null($decoded)) {
			$response['error'] = "Invalid JSON.";
			$response['request'] = $_REQUEST['json'];
		}
		
		else if (!extension_loaded('pgsql')) {
			$response['error'] = "Couldn\'t load PgSql extension.";
			$response['request'] = $_REQUEST['json'];
			
			
		} else { 
			$response['request'] = $decoded;
			readDB($decoded, $response);
		}
	} else {
		$response['error'] = "No request sent. Put a request in a POST or GET variable named 'json'.";
	}
	
	if (!$response['omitValues'] && $isCSV && !isset($response['error'])) {
		header('Content-Disposition: attachment; filename="result.csv"');
		echo strtoupper(implode(";", $response['columnNames'])) . "\n";
		outputCSV($response['values']);
		exit();
	} else {
		$encoded = $prettyPrint ? prettyPrint(json_encode($response)) : json_encode($response);
		header('Content-type: application/json');
		exit('jsonCallback(' . $encoded . ');');
	}
}
?>