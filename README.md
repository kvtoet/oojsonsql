* Copyright 2013 Kaj Toet. See LICENSE.txt.

This script facilitates select queries with the use of JSON.
Currently implemented are wildcards, greater and smaller than, modulo, and some others.
Security is based on mysql_real_escape_string currently and comparing the tables, columns and databases by those available and allowed (inserted in an array). 
There are certain other strict rules applied, but do know this script is in alpha.

One can select to output as JSON or CSV. Both are automatically set with appropriate headers.

For MySQL, replace the PostgreSQL functions in database.inc.php with the appropriate MySQL functions.

The script is small, customizable and managable.

For tips or questions, email me at kvtoet, at, gmail, dot, com.

An example of a query:
	
	var whereArguments = { };
	
	if ($("#interfacetype").val()) {
		// Add where argument: WHERE interfacetype = ..
		$.extend(whereArguments, { 'interfacetype': $("#interfacetype").val() });
	}
	
	// AND..
	if ($("#locationrole").val()) {
		// Subquery. Similar to WHERE id = (SELECT node_id FROM nodelocation WHERE roleid = ..)
		$.extend(whereArguments, { 
			"id": {
				"columns": [
					"node_id"
				],
				"limit": 0,
				"table": "nodelocation",
				"where": {"roleid": $("#locationrole").val()}
			} 
		});
	}

	var jsonQuery = {
		// database to connect to
		"database": $("input[name='database']:checked").val(),
		// table to connect to
		"table": "nodes",
		// order results by column ..
		"orderby": "position", 
		// return following columns
		"columns": [ "position"], 
		// set the limit to 1000. (<=0 is unlimited)
		"limit": 1000, 
		// set distinct by column ..
		"distinctby": "position",
		// filter by where arguments
		"where": whereArguments,
		// make a pretty print out of the JSON.
		// adds whitespace at appropriate places.
		"prettyJSON": true,
		// print values as downloadable CSV file or not.
		"isCSV": false
	};

	$.ajax({
		async: false,
		// Currently using JSONP for browser access from other domains. Adjust 
		jsonpCallback: 'jsonCallback',
		contentType: "application/json",
		dataType: 'jsonp',
		url: 'requests/',
		data: {
			json: JSON.stringify(jsonQuery)
		},
		success: function( data ) {
			console.log(data);
			$( "#location" ).html("");
			
			// Records are returned as a 2 dimensional array in data['values']
			for ( var i = 0; i < data['values'].length; i++ ) {
				$( "#location" ).append( "<option value=\"" + data['values'][i][0] + "\">" + data['values'][i][0] + "</option>" );
			}
			$( "#location" ).show("slow");
		}
	});