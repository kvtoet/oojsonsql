<?php

function getDBUsername() {
	return ''; // set database username here
}

function getDBPassword() {
	return ''; // set database password here
}

function is_assoc($arr)
{
    return array_keys($arr) !== range(0, count($arr) - 1);
}

function filterColumns($array, $column) {
	$return = array();
	foreach($array as $subarray) {
		$return[] = $subarray[$column];
	}	
	return $return;
}

// JSON

function getAllowedTables() {
	return array("locationrole", "octalixnode", "nodelocation", "field", "fieldhistory");
}

function getAllowedDatabases() {
	return array("HHSANALYSIS", "HHSLOCATIONSUBSET");
}

function array_values_recursive($array) {
   $temp = array();
   
   if(is_array($array)) {
	   foreach ($array as $value) {
			   if(is_array($value)) { $temp[] = array_values_recursive($value); }
			   else { $temp[] = $value; }
	   }
   }
   return $temp;
}
		
function ensureValues($request, &$response, &$limit) {
	if (isset($request['table'])) 
		$response['table'] = $request['table'];
	else {
		$arrayRes = getAllowedTables();
		$response['table'] = $arrayRes[0];
	}
	
	if (!isset($request["limit"]) || is_nan($request["limit"])) {
		$return["limit"] = 100;
	} else {
		$return["limit"] = $request['limit'];
	}
	
	if ($return['limit'] <= 0) {
		$printLimit = "infinite";
	} else {
		$limit = " LIMIT " . $return["limit"];
		$printLimit = $return["limit"];
	}
	$response['limit'] = $printLimit;
}

function readOperatorAndAddQuotes(&$value, $addQuotes) {
	$operChar = substr($value, 0, 1);
	if ($operChar == ">" || $operChar == "<" || $operChar == "%" || $operChar == "!") {
		$value =  ($addQuotes ? "'" . mysql_real_escape_string(substr($value, 1)) . "'" : substr($value, 1));
		
		if ($operChar == "!")
			$operChar = "!=";
			
	} else if ($operChar == "*") {
		$value =  ($addQuotes ? "'%" . mysql_real_escape_string(substr($value, 1)) . "%'" : '%' . substr($value, 1) . '%');
		$operChar = "LIKE";
	} else {
		$value =  ($addQuotes ? "'" . mysql_real_escape_string($value) . "'" : $value);
		$operChar = "=";
	}
	
	return $operChar;
}

function findNames($columns) {
	$return = array();
	foreach($columns as $key => $omit) {
		if (ctype_alpha($key)) {
			$return[] = $key;
		}
	}
	return $return;
}

function createQuery($dbh, $return, &$response, $upperColumns, &$queryCount) {
	$queryCount++;
	
	$limit = "";
	ensureValues($return, $response, $limit);

	if (!in_array($response['table'], getAllowedTables())) {
		$response['error'][] = "Table unknown.";
		$response['request'] = $_REQUEST['json'];
		return;
	}
			
	$result = pg_query($dbh, "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" . $response['table'] . "'");
	$foundColumns = filterColumns(pg_fetch_all($result), "column_name");
	
	foreach($foundColumns as $value) {
		if (!((is_array($value)) or ($value instanceof Traversable))) {
			$upperColumns[] = $response['table'] . '.' . $value;
		}
	}
	$allowedColumns = array_merge($foundColumns, $upperColumns);
	
	$columns = isset($return['columns']) ? $return['columns'] : $foundColumns;
	$response['columnNames'] = $columns;
	foreach($response['columnNames'] as $key => $value) {
		if (((is_array($value)) or ($value instanceof Traversable))) {
			$response['columnNames'][$key] = "SUBQUERY";
		}
	}
	
	// Subqueries in columns
	foreach($columns as $key => $value) {
		if (((is_array($value)) or ($value instanceof Traversable))) {
			// Subquery
			if (is_assoc($value)) {
				$columns[$key] = "(" . createQuery($dbh, $value, $response['subqueries'][], $upperColumns, $queryCount) . ") AS query". $queryCount;
				unset($return['columns'][$key]);
			} else {
				$response['error'][] = "Cant have array in columns.";
			}
		} else {
			$columns[$key] = $value;
		}
	}
	
	if (empty($return['columns']))
		$columns = $foundColumns;
	else {
		$containsSearch = count(array_intersect($return['columns'], $allowedColumns)) == count($return['columns']);
		if (!$containsSearch) {
			$response['error'][] = "Invalid columns selected.";
			return;
		}
	}
	
	if (empty($return['distinctby'])) {
		$response['distinctby'] = "not unique";
	} else {
		$containsSearch = in_array($return['distinctby'], $allowedColumns);
		if (!$containsSearch) {
			$response['error'][] = "Invalid unique by selected.";
			return;
		}
		$response['distinctby'] = $return['distinctby'];
		foreach($columns as $key => $value) {
			if ($value == $response['distinctby']) {
				$columns[$key] = "DISTINCT " . $value;
			}
		}
	}
	
	if (empty($return['where'])) {
		$response['where'] = "unfiltered";
		$where = "";
	} else {
		$whereColumns = array_keys($return['where']);
		$containsSearch = count(array_intersect($whereColumns, $allowedColumns)) == count($whereColumns);
		if (!$containsSearch) {
			$response['error'][] = "Invalid where columns selected.";
			return;
		}
		$response['where'] = $return['where'];
		$where = " WHERE";
		$whereArray = array();
		foreach($response['where'] as $key => $value) {
			// IN(..) or Subquery
			if (((is_array($value)) or ($value instanceof Traversable))) {
				// Subquery
				if (is_assoc($value)) {
					$whereArray[] = " $key IN (" . createQuery($dbh, $value, $response['subqueries'][], $upperColumns, $queryCount) . ")";
				// IN(..)
				} else {					
					// check if array has operators and put them separately
					$operArray = array();
					foreach($value as $key2 => $iter2) {					
						$operChar = readOperatorAndAddQuotes($iter2, !in_array($iter2, $allowedColumns));
						if ($operChar != "=") {
							$whereArray[] = " $key $operChar $iter2";
							unset($value[$key2]);
						} else {
							$value[$key2] = $iter2;
						}
					}
					// implode non operator containing values or containing =
					if (!empty($value)) 
						$whereArray[] = " $key IN (" . implode(',', $value) . ")";
				}
			// = < > or LIKE
			} else {
				$operChar = readOperatorAndAddQuotes($value, !in_array($value, $allowedColumns));
				$whereArray[] = " $key $operChar $value";
			}				
		}
		$where .= implode(" AND ", $whereArray);
	}
	if (empty($return['orderby'])) {
		$order = "";
	} else if (is_array($return['orderby'])) {
		$containsSearch = count(array_intersect($return['orderby'], $allowedColumns)) == count($return['orderby']);
		if (!$containsSearch) {
			$response['error'][] = "Invalid order columns selected.";
			$order = "";
		} else {
			$response['orderby'] = $return['orderby'];
			$order = " ORDER BY " . implode(", ", $response['orderby']);
		}
	} else if (!in_array($return['orderby'], $foundColumns)) {
		$response['error'][] = "Invalid order column selected.";
		$order = "";
	} else {
		$response['orderby'] = $return['orderby'];
		$order = " ORDER BY " . $response["orderby"];
	}
	
	$query = "SELECT " . implode(",", $columns) . " FROM " . $response["table"] . $where . $order . $limit;
	
	$response['rawquery'] = $query;
	
	return $query;
}

function readDB($return, &$response) {
	$queryCount = 0;

	$response['omitValues'] = isset($return['omitValues']) && $return['omitValues'];
	$dbs = getAllowedDatabases();
	if (!isset($return['database']) || !in_array($return['database'], $dbs)) {
		$response['database'] = $dbs[0];
	} else {
		$response['database'] = $return['database'];
	}

	$dbh = pg_connect("host=localhost dbname=" . $response['database'] . " user=" . getDBUsername() . " password=" . getDBPassword() );
	
	if (!$dbh) {
		$response['error'][] = "Error in connection: " . pg_last_error();
		$response['request'] = $_REQUEST['json'];
		
	} else {
		if (!$response['omitValues']) {
			// Do the actual lookup
			$query = createQuery($dbh, $return, $response, array(), $queryCount);
			$result = pg_query($dbh, $query);
			if (!$result) {
				$response['error'][] = "Fault in query: " . pg_last_error($dbh);
				return;
			}
			$response['values'] = array_values_recursive(pg_fetch_all($result));
		} else
			createQuery($dbh, $return, $response, array(), $queryCount);
		
	}
}

?>